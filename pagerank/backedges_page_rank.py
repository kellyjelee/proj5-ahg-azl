from simple_page_rank import SimplePageRank

"""
This class implements the pagerank algorithm with
backwards edges as described in the second part of 
the project.
"""
class BackedgesPageRank(SimplePageRank):

    """
    The implementation of __init__ and compute_pagerank should 
    still be the same as SimplePageRank.
    You are free to override them if you so desire, but the signatures
    must remain the same.
    """

    """
    This time you will be responsible for implementing the initialization
    as well. 
    Think about what additional information your data structure needs 
    compared to the old case to compute weight transfers from pressing
    the 'back' button.
    """
    @staticmethod
    def initialize_nodes(input_rdd):
        # YOUR CODE HERE
        # The pattern that this solution uses is to keep track of 
        # (node, (weight, targets, old_weight)) for each iteration.
        # When calculating the score for the next iteration, you
        # know that 10% of the score you sent out from the previous
        # iteration will get sent back.
        def emit_edges(line):
            # ignore blank lines and comments
            if len(line) == 0 or line[0] == "#":
                return []
            # get the source and target labels
            source, target = tuple(map(int, line.split()))
            # emit the edge
            edge = (source, frozenset([target]))
            # also emit "empty" edges to catch nodes that do not have any
            # other node leading into them, but we still want in our list of nodes
            self_source = (source, frozenset())
            self_target = (target, frozenset())
            return [edge, self_source, self_target]

        # collects all outgoing target nodes for a given source node
        def reduce_edges(e1, e2):
            return e1 | e2 

        # sets the weight of every node to 0, and formats the output to the 
        # specified format of (source (weight, targets))
        def initialize_weights((source, targets)):
            oldweight = 1
            return (source, (1.0, oldweight, targets))

        nodes = input_rdd\
                .flatMap(emit_edges)\
                .reduceByKey(reduce_edges)\
                .map(initialize_weights)
        return nodes
        #return input_rdd.filter(lambda x: False)

    """
    You will also implement update_weights and format_output from scratch.
    You may find the distribute and collect pattern from SimplePageRank
    to be suitable, but you are free to do whatever you want as long
    as it results in the correct output.
    """
    @staticmethod
    def update_weights(nodes, num_nodes):
        # YOUR CODE HERE
        def distribute_weights((node, (weight, oldweight, targets))):
            retlist = [(node, (0.05*weight, weight, targets))]
            retlist += [(node, (0.1*oldweight, weight, []))]
            if len(targets) == 0:
                    for nod in range(0, num_nodes):
                        if nod != node:
                            retlist += [(nod, (0.85*weight/(num_nodes -1), weight, []))]
            for target in targets:
                factor = (weight*0.85) / len(targets)
                retlist += [(target, (factor, weight, []))]
            return retlist

        def collect_weights((node, values)):
            # YOUR CODE HERE
            targets = []
            weight = 0
            for value in values:
                weight += value[0]
                targets += value[2]
            return (node, (weight, weight, targets))

        return nodes\
                .flatMap(distribute_weights)\
                .groupByKey()\
                .map(collect_weights)

        #return nodes.filter(lambda x: False) 
                     
    @staticmethod
    def format_output(nodes): 
        # YOUR CODE HERE
        return nodes\
                .map(lambda (node, (weight, oldweight, targets)): (weight, node))\
                .sortByKey(ascending = False)\
                .map(lambda (weight, node): (node, weight))
        #return nodes.filter(lambda x: False) 
